package managedBeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import entities.Question;
import entities.Quiz;
import entities.Subject;
import services.QuestionService;
import services.QuizService;

@ManagedBean
@SessionScoped
public class UserQuizBean {
	private String title;
	private Subject subject;
	private Quiz qu;
	private List<Quiz> quizs;
	private List<Question> questions;
	private Question question;
	//private List<Integer> answers= new ArrayList<Integer>(); 
	private List<Integer> answers;
	private String msg ="",msg2 ="and the fcking list is :";
	private int ans=0;
	private int qnumber =1;
	float result =0;
	boolean passed = false;
	
	private int rate =0;
	
	
	
	

	
	
	
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public boolean isPassed() {
		return passed;
	}
	public void setPassed(boolean passed) {
		this.passed = passed;
	}
	public float getResult() {
		return result;
	}
	public void setResult(float result) {
		this.result = result;
	}
	public String getMsg2() {
		return msg2;
	}
	public void setMsg2(String msg2) {
		this.msg2 = msg2;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public int getQnumber() {
		return qnumber;
	}
	public void setQnumber(int qnumber) {
		this.qnumber = qnumber;
	}
	public int getAns() {
		return ans;
	}
	public void setAns(int ans) {
		this.ans = ans;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	public Quiz getQu() {
		return qu;
	}
	public void setQu(Quiz qu) {
		this.qu = qu;
	}
	
	public void setQuizs(List<Quiz> quizs) {
		this.quizs = quizs;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	public List<Integer> getAnswers() {
		
		return answers;
	}
	public void setAnswers(List<Integer> answers) {
		this.answers = answers;
	}
	

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}


	@EJB
	QuizService qzs;
	@EJB
	QuestionService qs;
	// choosing a quiz
	public List<Quiz> getQuizs() {
		quizs = qzs.getValidQuizzes();
		return quizs;

	}
	public int quNumber(int id) {
		return qzs.countQuizQuestions(id);
	}
	public float quDifficulty(int id) {
		return qzs.countQuizQuestionsdiff(id);
	}
	
	public String enterQuiz(Quiz q) {
		String navigateTo = "/pages/user/quizpage.xhtml";
		questions = qzs.getQuizQuestions(q.getId());
		answers= new ArrayList<Integer>();
		this.qu=q;
		this.passed=false;
		question = questions.get(0);
		qnumber = 1;
			
		
		return navigateTo;
		
	}
	
	/// in the choosen quiz now
	
	public String next() {
		String tmp ="Your answers are :";
		String tmp2 =" Correct answers are :";
		if(qnumber < questions.size()) {
			
			question = questions.get(qnumber);
			answers.add(ans);
			qnumber ++;
			msg += ans +"/";
			return "/pages/user/quizpage.xhtml";
		} else 
			msg +=ans;
		answers.add(ans);
		for(int i=0;i<questions.size();i++) {
			tmp += answers.get(i)+" /";
			tmp2 += questions.get(i).getAnswer()+" /";
		}
		msg2 =tmp +tmp2;
		
		return "/pages/user/answerpage.xhtml";
	}
	
	
	public String toResult() {
		int corr=0;
		float tmp =0;
		for(int i=0;i<questions.size();i++) {
			if(questions.get(i).getAnswer()==answers.get(i)) {
				corr++;
			}
		}
		tmp =(float)corr / questions.size();
		result=tmp*100;
		msg = new DecimalFormat("##.##").format(result);
		//result=corr;
		if(result>50) {
			this.passed=true;
		}
		
		return "/pages/user/resultpage.xhtml";
	}
	
	public void ratequiz() {
		qzs.rateQuiz(qu.getId(), rate);
	}
	

}
