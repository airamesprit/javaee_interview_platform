package managedBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import entities.Question;
import entities.Quiz;
import entities.Subject;
import services.QuestionService;
import services.QuizService;

@ManagedBean
@SessionScoped
public class QuizBean {
	private String title;
	private Subject subject;
	private Quiz qu;
	private List<Quiz> quizs;
	private List<Question> questions;
	private List<Question> myquestions;
	private int test=2;
	
	private Question quest;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Quiz getQu() {
		return qu;
	}

	public void setQu(Quiz qu) {
		this.qu = qu;
	}
	

	public List<Question> getMyquestions() {
		return myquestions;
	}

	public void setMyquestions(List<Question> myquestions) {
		this.myquestions = myquestions;
	}
	


	public Question getQuest() {
		return quest;
	}

	public void setQuest(Question quest) {
		this.quest = quest;
	}
	



	public int getTest() {
		return test;
	}

	public void setTest(int test) {
		this.test = test;
	}




	@EJB
	QuizService qzs;
	@EJB
	QuestionService qs;

	public void addQuiz() {
		qzs.addQuiz(new Quiz(title, subject));
	}

	public List<Quiz> getQuizs() {
		quizs = qzs.getAllQuizzes();
		return quizs;

	}
	
	public int quNumber(int id) {
		return qzs.countQuizQuestions(id);
	}
	public float quDifficulty(int id) {
		return qzs.countQuizQuestionsdiff(id);
	}
	//going to quiz management action
	public String manageQuiz(Quiz q) {
		String navigateTo = "/pages/admin/quizmanagement.xhtml";
		myquestions = qzs.getQuizQuestions(q.getId());
		this.qu=q;
		return navigateTo;
		
	}
	
	
	public List<Question> getQuestions(){
		questions = qs.getAllQuestion();
		return questions;
	}
	
	public void affectQuestion(Question q) {
		if (qzs.addQuestionToQuiz(qu.getId(), q)) {
			myquestions =qzs.getQuizQuestions(qu.getId());
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("added successfully !"));
		}
		else
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("already exists !"));
	}
	public String goToQuestion() {
		return "/pages/admin/question.xhtml";
	}
	
	public String updateQuizInfo() {
		qzs.updateQuiz(qu);
		return "/pages/admin/quiz.xhtml";
	}
	public void removeQuiz(Quiz q) {
		qzs.deleteQuiz(q);
	}
	public void retrieveQuestion(Question q) {
		qzs.removeQuestionfromQuiz(qu.getId(), q);
		myquestions =qzs.getQuizQuestions(qu.getId());
	}
	
	public String goToQuestionUp(Question q) {
		quest = q;
		return "/pages/admin/quesupdate.xhtml";
	}
	public void updateQuestion() {
		qs.updateQuestion(quest);
	}
	public void deleteQuestion() {
		qs.deleteQuestion(quest);
	}

}
