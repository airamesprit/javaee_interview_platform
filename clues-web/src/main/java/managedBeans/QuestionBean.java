package managedBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import entities.Question;
import services.QuestionService;

@ManagedBean
@SessionScoped
public class QuestionBean {
	private String questionContent;
	private int answer;
	private int difficulty;
	private Question question;
	private List<Question> questions;
	public String getQuestionContent() {
		return questionContent;
	}
	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}
	public int getAnswer() {
		return answer;
	}
	public void setAnswer(int answer) {
		this.answer = answer;
	}
	public int getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	@EJB
	QuestionService qs;
	
	public void addQuestion() {
		qs.addQuestion(new Question(questionContent,answer,difficulty));
	}
	
	
	
	public String backtoQuiz() {
		 return "/pages/admin/quiz.xhtml";
	}

}
