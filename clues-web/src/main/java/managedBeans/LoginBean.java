package managedBeans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import entities.AppUser;
import entities.Role;
import services.AppUserService;

@ManagedBean
@SessionScoped
public class LoginBean {
	private String login;
	private String password;
	private AppUser emp;
	private boolean loggedIn;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public AppUser getEmp() {
		return emp;
	}
	public void setEmp(AppUser emp) {
		this.emp = emp;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	@EJB
	AppUserService as;
	
	public String doLogin() {
		String navigateTo = null;
		emp = as.getUserByInfo(login, password);
		if(emp!=null && emp.getRole()==Role.PROJECT_CHIEF) {
			navigateTo="/pages/admin/quiz.xhtml";
			loggedIn=true;
		}
		else if(emp!=null && emp.getRole()==Role.CANDIDATE) {
			navigateTo="/pages/user/choosequiz.xhtml";
			loggedIn=true;
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("bad credentials"));
		}
		return navigateTo;
	}
	
	public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/pages/login?faces-redirect=true";
	}

}
