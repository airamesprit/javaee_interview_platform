package managedBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import entities.Role;
import entities.Subject;

@ManagedBean
@ApplicationScoped
public class MyData {
	private Subject[] subjects;
	
	private Role[] roles ;
	
	public Role[] getRoles() {
		return Role.values();
		
	}
	
	public Subject[] getSubjects() {
		return Subject.values();
	}
	

}
