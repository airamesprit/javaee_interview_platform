package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class ApprovedPK implements Serializable{
	@Temporal(TemporalType.DATE)
	Date passage_date;
	private int score;
	public ApprovedPK() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Date getPassage_date() {
		return passage_date;
	}
	public void setPassage_date(Date passage_date) {
		this.passage_date = passage_date;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((passage_date == null) ? 0 : passage_date.hashCode());
		result = prime * result + score;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApprovedPK other = (ApprovedPK) obj;
		if (passage_date == null) {
			if (other.passage_date != null)
				return false;
		} else if (!passage_date.equals(other.passage_date))
			return false;
		if (score != other.score)
			return false;
		return true;
	}

	

}
