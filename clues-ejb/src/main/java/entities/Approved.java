package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Approved implements Serializable {
	@EmbeddedId
	private ApprovedPK id;
	@ManyToOne
	@JoinColumn(name = "quizid" , referencedColumnName = "id" , insertable = false, updatable = false)
	private Quiz quiz;
	
	@ManyToOne
	@JoinColumn(name = "candidatid" , referencedColumnName = "id" , insertable = false, updatable = false)
	private AppUser candidat;
	
	@Temporal(TemporalType.DATE)
	Date date_entretient;

	public Approved() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ApprovedPK getId() {
		return id;
	}

	public void setId(ApprovedPK id) {
		this.id = id;
	}

	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	public AppUser getCandidat() {
		return candidat;
	}

	public void setCandidat(AppUser candidat) {
		this.candidat = candidat;
	}

	public Date getDate_entretient() {
		return date_entretient;
	}

	public void setDate_entretient(Date date_entretient) {
		this.date_entretient = date_entretient;
	}
	
	

}
