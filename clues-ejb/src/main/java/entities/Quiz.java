package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Quiz implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	@Enumerated(EnumType.STRING)
	private Subject subject;
	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.REMOVE},fetch=FetchType.EAGER)
	private List<Question> questions;
	@OneToMany
	private List<Rating> ratings;
	@OneToMany(mappedBy = "quiz")
	private List<Approved> approveds;
	
	public Quiz() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Quiz(String title, Subject subject) {
		super();
		this.title = title;
		this.subject = subject;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public List<Rating> getRatings() {
		return ratings;
	}
	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public List<Approved> getApproveds() {
		return approveds;
	}
	public void setApproveds(List<Approved> approveds) {
		this.approveds = approveds;
	} 
	
	
	
	
	

}
