package services;

import javax.ejb.Remote;

import entities.AppUser;

@Remote
public interface AppUserServiceRemote {
	public AppUser getUserByInfo(String mail,String password);

}
