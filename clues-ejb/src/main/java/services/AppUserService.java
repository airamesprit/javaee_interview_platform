package services;

import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.AppUser;

@LocalBean
@Stateless
public class AppUserService implements AppUserServiceRemote {
	@PersistenceContext(unitName="clues")
	EntityManager em;

	@Override
	public AppUser getUserByInfo(String mail, String password) {
		TypedQuery<AppUser> query = em.createQuery("select e from AppUser e"
				+" where e.mail=:email and"
				+" e.password=:password",AppUser.class);
		query.setParameter("email", mail);
		query.setParameter("password", password);
		AppUser emp = null;
		try {
			emp = query.getSingleResult();
			
		} catch (NoResultException e) {
			Logger.getAnonymousLogger().info("aucun utilisateur trouver avec le mail  " +mail);
		}
		return emp;
	}

}
