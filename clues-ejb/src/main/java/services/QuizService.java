package services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.Question;
import entities.Quiz;
import entities.Rating;

@Stateless
@LocalBean
public class QuizService implements QuizServiceRemote{
	@PersistenceContext(unitName="clues")
	EntityManager em;

	@Override
	public int addQuiz(Quiz qu) {
		em.persist(qu);
		return qu.getId();
	}

	@Override
	public List<Quiz> getAllQuizzes() {
		TypedQuery<Quiz> query = em.createQuery("select qu from Quiz qu ",Quiz.class);
		return query.getResultList();
	}

	@Override
	public int countQuizQuestions(int id) {
		List<Question> l = new ArrayList<Question>();
		Quiz q = new Quiz();
		q = em.find(Quiz.class, id);
		l =q.getQuestions();
		return l.size();
	}

	@Override
	public float countQuizQuestionsdiff(int id) {
		List<Question> l = new ArrayList<Question>();
		float t =0;
		Quiz q = new Quiz();
		q = em.find(Quiz.class, id);
		l =q.getQuestions();
		if(l.size()>0) {
			for(int i=0;i<l.size();i++) {
				t +=l.get(i).getDifficulty();
			}
			return t/l.size();
		}return -1;
	}

	@Override
	public List<Question> getQuizQuestions(int id) {
		List<Question> l = new ArrayList<Question>();
		Quiz q = new Quiz();
		q = em.find(Quiz.class, id);
		l =q.getQuestions();
		return l;
	}

	@Override
	public boolean addQuestionToQuiz(int id,Question ques) {
		boolean test = false;
		List<Question> l = new ArrayList<Question>();
		Quiz q = new Quiz();
		q = em.find(Quiz.class, id);
		l =q.getQuestions();
		for(int i=0 ; i < l.size() ;i++) {
			if(l.get(i).getId()==ques.getId()) {
				System.out.println("already added");
				
				return false;
			}}
				l.add(ques);
			q.setQuestions(l);
			em.merge(q);
			System.out.println("already adding to " +q.getTitle());
			test = true;
			
		
		return test;
	}

	@Override
	public void updateQuiz(Quiz q) {
		em.find(Quiz.class, q.getId());
		em.merge(q);
		
	}

	@Override
	public void deleteQuiz(Quiz q) {
		
		em.remove(em.find(Quiz.class, q.getId()));
		
	}

	@Override
	public boolean removeQuestionfromQuiz(int id, Question ques) {
		boolean test = false;
		List<Question> l = new ArrayList<Question>();
		Quiz q = new Quiz();
		q = em.find(Quiz.class, id);
		l =q.getQuestions();
		for(int i=0 ; i < l.size() ;i++) {
			if(l.get(i).getId()==ques.getId()) {
				System.out.println("found !");
				l.remove(l.get(i));
				q.setQuestions(l);
				em.merge(q);
				return true;
			}}
		
		return test;
	}

	@Override
	public List<Quiz> getValidQuizzes() {
		TypedQuery<Quiz> query = em.createQuery("select qu from Quiz qu ",Quiz.class);
		List<Quiz> l = new ArrayList<Quiz>();
		List<Quiz> vl = new ArrayList<Quiz>();
		l =query.getResultList();
		for(int i=0 ; i < l.size() ;i++) {
			if(countQuizQuestions(l.get(i).getId()) > 2) {
				vl.add(l.get(i));
			}}
		
		return vl;
	}

	@Override
	public void rateQuiz(int id, int rate) {
		Rating r = new Rating(rate);
		em.persist(r);
		Quiz q = new Quiz();
		q = em.find(Quiz.class, id);
		List<Rating> l =new ArrayList<Rating>();
		l = q.getRatings();
		l.add(r);
		q.setRatings(l);
		em.merge(q);
		
	}
	

}
