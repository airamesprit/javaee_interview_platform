package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.Question;
import entities.Quiz;

@Stateless
@LocalBean
public class QuestionService implements QuestionServiceRemote{
	@PersistenceContext(unitName="clues")
	EntityManager em;

	@Override
	public int addQuestion(Question q) {
		em.persist(q);
		return q.getId();
	}

	@Override
	public List<Question> getAllQuestion() {
		TypedQuery<Question> query = em.createQuery("select qu from Question qu ",Question.class);
		return query.getResultList();
	}

	@Override
	public void updateQuestion(Question q) {
		em.merge(q);
		
	}

	@Override
	public void deleteQuestion(Question q) {
		em.remove(em.find(Question.class, q.getId()));
		
	}

}
