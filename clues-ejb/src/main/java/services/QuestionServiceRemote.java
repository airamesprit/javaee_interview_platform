package services;

import java.util.List;

import javax.ejb.Remote;

import entities.Question;

@Remote
public interface QuestionServiceRemote {
	public int addQuestion(Question q);
	public List<Question> getAllQuestion();
	public void updateQuestion(Question q);
	public void deleteQuestion(Question q);
}
