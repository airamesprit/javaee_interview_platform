package services;

import java.util.List;

import javax.ejb.Remote;

import entities.Question;
import entities.Quiz;

@Remote
public interface QuizServiceRemote {
	public int addQuiz(Quiz qu);
	public List<Quiz> getAllQuizzes();
	public List<Quiz> getValidQuizzes();
	public int countQuizQuestions(int id);
	public float countQuizQuestionsdiff(int id);
	public List<Question> getQuizQuestions(int id);
	public boolean addQuestionToQuiz(int id,Question q);
	public void updateQuiz(Quiz q);
	public void deleteQuiz(Quiz q);
	public boolean removeQuestionfromQuiz(int id,Question q);
	
	public void rateQuiz(int id,int rate);
	

}
